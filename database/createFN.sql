drop table if exists Type cascade;

CREATE TABLE Type(
idType serial PRIMARY KEY,
label varchar(35)
);

delete from type;
INSERT INTO type (label) SELECT distinct(trim(REGEXP_REPLACE(objectname,'( ){2,}',' ')))from museum;
--------------------------------------
drop table if exists Artist cascade;

CREATE TABLE Artist(
idArtist serial PRIMARY KEY,
name text,
role text
);

delete from artist;
INSERT INTO Artist (name, role) SELECT distinct (trim(REGEXP_REPLACE(artistdisplayname,'( ){2,}',' '))), (trim(REGEXP_REPLACE(artistrole,'( ){2,}',' '))) from museum;
--------------------------------------
drop table if exists Era cascade;

CREATE TABLE Era(
idEra serial PRIMARY KEY,
Period varchar(255),
reign varchar(255)
);

delete from era;
INSERT INTO Era(period, reign) SELECT distinct (trim(REGEXP_REPLACE(period,'( ){2,}',' '))), (trim(REGEXP_REPLACE(reign,'( ){2,}',' '))) from museum;
--------------------------------------
drop table if exists Department cascade;

create table department(
idDepartment serial primary key,
name varchar(255));

delete from department;
INSERT INTO Department (name) SELECT distinct Department from museum;
--------------------------------------
drop table if exists Location cascade;

CREATE TABLE Location (
idLocation serial PRIMARY KEY,
city varchar(255),
zone varchar(255)
);

delete from Location;
INSERT INTO Location (city, zone) SELECT distinct (trim(REGEXP_REPLACE(city,'( ){2,}',' '))), (trim(REGEXP_REPLACE(country,'( ){2,}',' '))) from museum;
--------------------------------------
drop table if exists Medium cascade;

CREATE TABLE Medium(
idMedium serial PRIMARY KEY,
label text
);

delete from medium;
INSERT INTO Medium (label) SELECT distinct (trim(REGEXP_REPLACE(medium,'( ){2,}',' '))) from museum;
--------------------------------------
drop table if exists Object cascade;

CREATE TABLE Object(
objectid serial PRIMARY KEY,
reference varchar(255),
title text,
idType integer,
idDepartment integer,
idArtist integer,
idMedium integer,
idEra integer,
idLocation integer,
dateBegin integer,
dateEnd integer,
isHighlight boolean,
isPublicDomain boolean,
accessionyear integer,
linkressource text,
FOREIGN KEY (idType) REFERENCES Type (idType),
FOREIGN KEY (idMedium) REFERENCES Medium (idMedium),
FOREIGN KEY (idArtist) REFERENCES Artist (idArtist),
FOREIGN KEY (idLocation) REFERENCES Location (idLocation),
FOREIGN KEY (idDepartment) REFERENCES Department (idDepartment),
FOREIGN KEY (idEra) REFERENCES Era (idEra)
);

delete from object;
INSERT INTO object(
reference,
title,
idType,
idDepartment,
idArtist ,
idMedium,
idEra ,
idLocation ,
dateBegin ,
dateEnd ,
isHighlight ,
isPublicDomain ,
accessionyear ,
linkressource ) 
SELECT distinct 
m.objetnumber,
m.title,
t.idType,
d.idDepartment,
a.idArtist ,
mm.idMedium,
e.idEra ,
l.idLocation ,
m.objectbegindate ,
m.objectenddate ,
m.isHighlight ,
m.isPublicDomain ,
cast(m.accessionyear as integer),
m.linkresource
FROM museum m, location l, type t, department d, era e, artist a, medium mm
where l.city=m.city 
and l.zone=m.country 
and t.label=m.objectName
and d.name=m.department 
and e.period=m.period
and e.reign=m.reign
and a.name = m.artistdisplayname
and m.medium=mm.label;


