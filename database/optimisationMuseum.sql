ALTER TABLE museum
DROP COLUMN istimelinework,
DROP COLUMN gallerynumber,
DROP COLUMN constituentId,
DROP COLUMN artistPrefix,
DROP COLUMN artistDisplayBio,
DROP COLUMN artistSuffix,
DROP COLUMN artistAlphaSort,
DROP COLUMN artistNationality,
DROP COLUMN artistBeginDate,
DROP COLUMN artistEndDate,
DROP COLUMN portfolio,
DROP COLUMN artistgender,
DROP COLUMN artistulanurl,
DROP COLUMN artistwikidataurl,
DROP COLUMN creditline,
DROP COLUMN geographytype,
DROP COLUMN state,
DROP COLUMN subregion,
DROP COLUMN locus,
DROP COLUMN excavation,
DROP COLUMN rightsandreproduction,
DROP COLUMN objectwikidataurl,
DROP COLUMN metadatadate,
DROP COLUMN repository,
DROP COLUMN tagsaaturl,
DROP COLUMN tagswikidataurl,
DROP COLUMN objectDate,
DROP Column locale,
DROP Column river,
DROP Column dynasty,
drop column tags;

update museum set city = LOWER(city);
update museum set artistdisplayname = LOWER(artistdisplayname);
update museum set artistRole = LOWER(artistRole);
update museum set objectname = LOWER(objectname);
update museum set period = LOWER(period);
update museum set reign = LOWER(reign);
update museum set culture = LOWER(culture);
update museum set county = LOWER(county);	
update museum set country = LOWER(country);
update museum set region = LOWER(region);
update museum set medium = LOWER(medium);	

delete from museum where culture like 'probably' 
	or period like 'probably' 
	or artistdisplayname like 'probably' 
	or objectname like 'probably' 
	or city like 'probably' 
	or county like 'probably' 
	or country like 'probably' 
	or region like 'probably' 
	or reign like 'probably' 
	or medium like 'probably' ;

delete from museum where culture like 'possibly' 
	or period like 'possibly' 
	or artistdisplayname like 'possibly' 
	or objectname like 'possibly' 
	or city like 'possibly' 
	or county like 'possibly' 
	or country like 'possibly' 
	or region like 'possibly' 
	or reign like 'possibly' 
	or medium like 'possibly' ;

delete from museum where culture like '?' 
	or period like '?' 
	or reign like '?' 
	or artistdisplayname like '?' 
	or objectname like '?' 
	or city like '?' 
	or county like '?' 
	or country like '?' 
	or region like '?' 
	or medium like '?';

delete from museum where culture like ' or ' 
	or period like ' or ' 
	or artistdisplayname like ' or ' 
	or objectname like ' or ' 
	or city like ' or ' 
	or county like ' or ' 
	or country like ' or ' 
	or reign like ' or ' 
	or region like ' or ' ;


delete from museum where city is null and county is null and country is null and region is null and culture is null;
update museum set country = region where country is null;
update museum set objectname = classification where objectname is null;

delete from museum where objectname is null;
alter table museum drop column classification;

update museum set culture=replace(culture, 'peoples', '') where culture like '%peoples%';
update museum set culture=replace(culture, 'people', '') where culture like '%people%';

update museum set objectname = REPLACE(objectname,'ā', 'a') where objectname like '%ā%';
update museum set objectname = REPLACE(objectname,'-', '')  where objectname like '%-%';
update museum set objectname = REPLACE(objectname,'/', '')  where objectname like '%/%';
update museum set objectname = REPLACE(objectname,'_', '') where objectname like '%_%';
update museum set objectname = REPLACE(objectname,';', '') where objectname like '%;%';
update museum set objectname = REPLACE(objectname,'.', '') where objectname like '%.%';
update museum set objectname = REPLACE(objectname,',', '') where objectname like '%,%';
update museum set objectname = REPLACE(objectname,'ó', 'o') where objectname like '%ó%';
update museum set objectname = REPLACE(objectname,'[', ' ') where objectname like '%[%';
update museum set objectname = REPLACE(objectname,']', ' ') where objectname like '%]%';
update museum set objectname = REGEXP_REPLACE(objectname,'( ){2,}',' ');
update museum set objectname = REGEXP_REPLACE(objectname,'\(.*\)','');
delete from museum where length(objectname)>30;
delete from museum where objectname similar to '[[:alpha:]]';
delete from museum where objectname similar to '%([[:digit:]])%';
update museum set objectname = trim(objectname);

update museum set artistdisplayname = REGEXP_REPLACE(artistdisplayname,'\(.*\)','');
update museum set artistdisplayname = REPLACE(artistdisplayname,'-', ' ') where artistdisplayname like '%-%';
update museum set artistdisplayname = REPLACE(artistdisplayname,'_', ' ') where artistdisplayname like '%_%'; 
update museum set artistdisplayname = REPLACE(artistdisplayname,';', ' ') where artistdisplayname like '%;%';
update museum set artistdisplayname = REPLACE(artistdisplayname,',', ' ') where artistdisplayname like '%,%';
update museum set artistdisplayname = REPLACE(artistdisplayname,'|', ', ') where artistdisplayname like '%|%';
update museum set artistdisplayname = REPLACE(artistdisplayname,', ', '') where artistdisplayname like ',%';
update museum set artistdisplayname = REPLACE(artistdisplayname,'[', ', ') where artistdisplayname like '%[%';
update museum set artistdisplayname = REPLACE(artistdisplayname,']', ', ') where artistdisplayname like '%]%';
update museum set artistdisplayname = REPLACE(artistdisplayname,'"', ' ') where artistdisplayname like '%"%';
update museum set artistdisplayname = REGEXP_REPLACE(artistdisplayname,'\(.*\)','');
delete from museum where artistdisplayname similar to '%([[:digit:]])%';
update museum set artistdisplayname = REGEXP_REPLACE(artistdisplayname,'( ){2,}',' ');
update museum set artistdisplayname = trim(artistdisplayname);

update museum set artistRole = REGEXP_REPLACE(artistRole,'\(.*\)','');
update museum set artistRole = REPLACE(artistRole,'-', ' ') where artistRole like '%-%';
update museum set artistRole = REPLACE(artistRole,'_', ' ') where artistRole like '%_%';
update museum set artistRole = REPLACE(artistRole,';', ' ') where artistRole like '%;%';
update museum set artistRole = REPLACE(artistRole,'.', ' ') where artistRole like '%.%';
update museum set artistRole = REPLACE(artistRole,',', ' ') where artistRole like '%,%';
update museum set artistRole = REPLACE(artistRole,'|', ', ') where artistRole like '%|%';
update museum set artistRole = REGEXP_REPLACE(artistRole,'( ){2,}',' ');
delete from museum where artistRole similar to '%([[:digit:]])%';
update museum set artistRole = trim(artistRole);

delete from museum where period like '%dates being researched%';
delete from museum where period like '%date being researched%';
delete from museum where period = 'late';
update museum set period = REPLACE(period,'ō', 'o') where period like 'ō';
update museum set period = REGEXP_REPLACE(period,'\(.*\)','');
update museum set period = REPLACE(period,'period', ' ') where period like 'period';
update museum set period = REGEXP_REPLACE(period,'(([[:alpha:]]{3,} dynasty )|dynasty [[:alpha:]]+)', ' ');
update museum set period = REPLACE(period,'|', ' ') where period like '%|%';
update museum set period = REPLACE(period,':', ' ') where period like '%:%';
update museum set period = REPLACE(period,'-', ' ') where period like '%-%';
update museum set period = REPLACE(period,'_', ' ') where period like '%_%';
update museum set period = REPLACE(period,';', ' ') where period like '%;%';
update museum set period = REPLACE(period,'–', ' ') where period like '%–%';
update museum set period = REPLACE(period,'.', '') where period like '%.%';
update museum set period = REPLACE(period,'/', ' ') where period like '%/%';
update museum set period = REPLACE(period,',', ' ')where period like '%,%';
update museum set period = REGEXP_REPLACE(period,'( ){2,}',' ');
update museum set period = trim(period);

update museum set reign = REPLACE(reign,'reign of', ' ');
update museum set reign = REPLACE(reign,'reigns of', ' ');
update museum set reign = REGEXP_REPLACE(reign,'\(.*\)','');
update museum set reign = REPLACE(reign,'|', ' ') where reign like '%|%';
update museum set reign = REPLACE(reign,':', ' ') where reign like '%:%';
update museum set reign = REPLACE(reign,'-', ' ') where reign like '%-%';
update museum set reign = REPLACE(reign,'_', ' ')where reign like '%_%';
update museum set reign = REPLACE(reign,';', ' ')where reign like '%;%';
update museum set reign = REPLACE(reign,'–', ' ')where reign like '%–%';
update museum set reign = REPLACE(reign,'.', '')where reign like '%.%';
update museum set reign = REPLACE(reign,'/', ' ')where reign like '%/%';
update museum set reign = REPLACE(reign,',', ' ')where reign like '%,%';
update museum set reign = REGEXP_REPLACE(reign,'( ){2,}',' ');
update museum set reign = trim(reign);

update museum set culture = REGEXP_REPLACE(culture,'\(.*\)','');
update museum set culture = REPLACE(culture,'group', '') where culture like '%group%';
update museum set culture = REPLACE(culture,'è', 'e') where culture like 'è';
update museum set culture = REPLACE(culture,'after%', '') where culture like '%after%';
update museum set culture = REPLACE(culture,' for %', '')  where culture like '% for %';
update museum set culture = REGEXP_REPLACE(culture,'([[:digit:]])','');
update museum set culture = REPLACE(culture,'|', ' ') where culture like '%|%';
update museum set culture = REPLACE(culture,'"', '') where culture like '%"%';
update museum set culture = REPLACE(culture,':', ' ') where culture like '%:%';
update museum set culture = REPLACE(culture,'-', ' ') where culture like '%-%';
update museum set culture = REPLACE(culture,'_', ' ') where culture like '%_%';
update museum set culture = REPLACE(culture,';', ' ') where culture like '%;%';
update museum set culture = REPLACE(culture,'–', ' ') where culture like '%–%';
update museum set culture = REPLACE(culture,'.', '') where culture like '%.%';
update museum set culture = REPLACE(culture,'/', ' ') where culture like '%/%';
update museum set culture = REPLACE(culture,',', ' ') where culture like '%,%';
update museum set culture = REGEXP_REPLACE(culture,'( ){2,}',' ');
update museum set culture = trim(culture);

update museum set city = REPLACE(city,'|', ' ') where city like '%|%';
delete from museum where city = 'n l ';
update museum set city = REPLACE(city,'á', 'a') where city like '%á%';
update museum set city = REPLACE(city,'ó', 'o') where city like '%ó%';
update museum set city = REPLACE(city,'and', '') where city like '%and%';
update museum set city = REGEXP_REPLACE(city,'\(.*\)','');
update museum set city = REPLACE(city,'village', '') where city like '%village%';
update museum set city = REPLACE(city,'region', '') where city like '%region%';
update museum set city = REPLACE(city,'nd', '') where city like '%nd%';
update museum set city = REPLACE(city,'n d', '') where city like '%n d%';
update museum set city = REGEXP_REPLACE(city,'/([[:digit:]])/g','');
update museum set city = REPLACE(city,':', ' ') where city like '%:%';
update museum set city = REPLACE(city,'-', ' ') where city like '%-%';
update museum set city = REPLACE(city,'[', ' ') where city like '%[%';
update museum set city = REPLACE(city,']', ' ') where city like '%]%';
update museum set city = REPLACE(city,'_', ' ') where city like '%_%';
update museum set city = REPLACE(city,';', '') where city like '%;%';
update museum set city = REPLACE(city,'–', ' ') where city like '%–%';
update museum set city = REPLACE(city,'.', ' ') where city like '%.%';
update museum set city = REPLACE(city,'/', ' ') where city like '%/%';
update museum set city = REPLACE(city,',', ' ') where city like '%,%';
update museum set city = REGEXP_REPLACE(city,'( ){2,}',' ');
update museum set city = trim(city);

update museum set country = REPLACE(country,':%', '') where country like '%:%';
update museum set country = REPLACE(country,'region', ' ') where country like '%region%';
update museum set country = REGEXP_REPLACE(country,'\(.*\)','');
update museum set country = REPLACE(country,'-', ' ') where country like '%-%';
update museum set country = REGEXP_REPLACE(country,'(\|[[:alpha:]]*)', ' ');
update museum set country = REPLACE(country,'district', '') where country like '%district%';
update museum set country = REPLACE(country,'ô', 'o') where country like '%ô%';
update museum set country = REPLACE(country,'dem. rep.', '') where country like '%dem. rep.%';
update museum set country = REPLACE(country,'democratic republic of ', '') where country like '%democratic republic of %';
update museum set country = REPLACE(country,'democratic republic of the ', ' ') where country like '%democratic republic of the%';
update museum set country = REPLACE(country,'republic of the ', ' ') where country like '%republic of the%';
update museum set country = REPLACE(country,',', ' ') where country like '%,%';
delete from museum where country = 'east';
delete from museum where country = 'west';
delete from museum where country = 'north';
delete from museum where country = 'sud';
delete from museum where country like 'lisez et propagez nos %';
update museum set country = REPLACE(country,'present day', ' ') where country like '%present day%';
update museum set country = REPLACE(country,' and ', ' ') where country like '% and %';
update museum set country = REPLACE(country,'present-day', ' ') where country like '%present-day%';
update museum set country = REGEXP_REPLACE(country,'/([[:digit:]])/g','');
delete from museum where country like '%unknown%';
update museum set country = REPLACE(country,'us', 'usa') where country like '%us%';
update museum set country = REPLACE(country,'&', ' ') where country like '%&%';
update museum set country = REPLACE(country,'united states', 'usa') where country like '%united states%';
update museum set country = REGEXP_REPLACE(country,'( ){2,}',' ');
update museum set country = trim(country);

delete from museum where length(medium)>30;
update museum set medium = REPLACE(medium,' and ', '') where medium like '% and %';
delete from museum where medium like '%available%';
delete from museum where medium like '%apparently%';
update museum set medium = REPLACE(medium,'é', 'e') where medium like '%é%';
update museum set medium = REPLACE(medium,' with ', ' ') where medium like '% with %';
update museum set medium = REGEXP_REPLACE(medium,'\(([[:alpha:]])\)|([[:alpha:]])\)','', 'g');
update museum set medium = REPLACE(medium,')', ' ') where medium like '%)%';
update museum set medium = REPLACE(medium,' a ', '') where medium like '% a %';
update museum set medium = REPLACE(medium,' b ', '') where medium like '% b %';
update museum set medium = REPLACE(medium,' c ', '') where medium like '% c %';
update museum set medium = REPLACE(medium,' d ', '') where medium like '% d %';
delete from museum where medium like '% l %';
delete from museum where medium like '% f %';
delete from museum where medium like '% j %';
delete from museum where medium like '% i %';
delete from museum where medium like '% h %';
delete from museum where medium like '% e %';
delete from museum where medium similar to '%[[:digit:]]%';
update museum set medium = REPLACE(medium,'a ', '') where medium like 'a %';
update museum set medium = REPLACE(medium,'-', ' ') where medium like '%-%';
update museum set medium = replace(replace(medium,CHR(10),''),CHR(13),'');
update museum set medium = REPLACE(medium,':', ' ') where medium like '%:%';
update museum set medium = REPLACE(medium,'-', ' ') where medium like '%-%';
update museum set medium = REPLACE(medium,'[', ' ') where medium like '%[%';
update museum set medium = REPLACE(medium,']', ' ') where medium like '%]%';
update museum set medium = REPLACE(medium,'_', ' ') where medium like '%_%';
update museum set medium = REPLACE(medium,';', '') where medium like '%;%';
update museum set medium = REPLACE(medium,'–', ' ') where medium like '%–%';
update museum set medium = REPLACE(medium,'.', ' ') where medium like '%.%';
update museum set medium = REPLACE(medium,'/', ' ') where medium like '%/%';
update museum set medium = REPLACE(medium,',', ' ') where medium like '%,%';
update museum set medium = REPLACE(medium,'"', ' ') where medium like '%"%';
update museum set medium = REPLACE(medium,'—', ' ') where medium like '%—%';
update museum set medium = REGEXP_REPLACE(medium,'( ){2,}',' ');
update museum set medium = trim(medium);

update museum set culture = 'null' where culture is null;
update museum set period = 'null' where period is null;
update museum set reign = 'null' where reign is null;
update museum set artistrole = 'null' where artistrole is null;
update museum set artistdisplayname = 'null' where artistdisplayname is null;
update museum set city = 'null' where city is null;
update museum set country = 'null' where country is null;
update museum set medium = 'null' where medium is null;
update museum set objectname = 'null' where objectname is null;
