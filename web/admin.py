from django.contrib import admin

# Register your models here.
from web.models import Era, Department, Artist, Location, Medium, Object, Type

admin.site.register(Artist)
admin.site.register(Department)
admin.site.register(Era)
admin.site.register(Location)
admin.site.register(Medium)
admin.site.register(Object)
admin.site.register(Type)


