from django.shortcuts import render
from rest_framework import viewsets
from django.views.generic import TemplateView
from web.serializers import EraSerializer, MediumSerializer, LocationSerializer, DepartmentSerializer, TypeSerializer, ArtistSerializer
from web.models import Era, Medium, Location, Department, Type, Artist, Object


class index(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super(index, self).get_context_data(**kwargs)
        context["carousel"] = Object.objects.raw("select objectid, reference, linkressource, iddepartment, datebegin, dateend, idtype, title, o.idlocation, l.zone, a.name from Object o join location l on l.idlocation = o.idlocation join artist a on a.idartist = o.idartist where ishighlight = true order by random() limit 5")
        return context


class department(TemplateView):
    template_name = 'department.html'

    def get_context_data(self, **kwargs):
        context = super(department, self).get_context_data(**kwargs)

        obj = str(self.request).split('?')
        if len(obj) == 2:
            obj = obj[1].split("'")[0]
            context["search"] = Department.objects.raw("select objectid, reference,linkressource, iddepartment, datebegin, dateend, idtype, title, l.zone, a.name from Object o join location l on l.idlocation = o.idlocation join artist a on a.idartist = o.idartist where iddepartment = "+obj+" order by random() limit 5")

        print(obj)

        context["histogram"] = Department.objects.raw("select d.iddepartment, d.name, count(o.objectid) from department d join object o on o.iddepartment = d.iddepartment where o.iddepartment is not null group by d.iddepartment order by count desc")
        context["table"] = Department.objects.all()[:50]
        return context


class artist(TemplateView):
    template_name = 'artist.html'

    def get_context_data(self, **kwargs):
        context = super(artist, self).get_context_data(**kwargs)
        context["chart"] = Artist.objects.raw("select distinct(a.name), 1 as idartist, count(o.objectid) from artist a join object o on o.idartist = a.idartist where a.name not like 'null' group by a.idartist order by count desc limit 15")
        return context

class medium(TemplateView):
    template_name = 'medium.html'

    def get_context_data(self, **kwargs):
        context = super(medium, self).get_context_data(**kwargs)
        context["chart"] = Medium.objects.raw("select distinct(label), 1 as idmedium, count(o.objectid) from medium m join object o on o.idmedium = m.idmedium group by m.idmedium order by count desc limit 15;")
        # context["table"] = Medium.objects.all()[:50]
        return context


class culture(TemplateView):
    template_name = 'culture.html'

    def get_context_data(self, **kwargs):
        context = super(culture, self).get_context_data(**kwargs)
        context["histogram"] = Location.objects.raw("select distinct(zone), 1 as idlocation, count(o.objectid) from location l join object o on o.idlocation = l.idlocation where zone not like 'null' group by l.zone order by count desc limit 15;")
        return context


def login(request):
    return render(request, 'login.html', {})


class epoque(TemplateView):
    template_name = 'epoque.html'

    def get_context_data(self, **kwargs):
        context = super(epoque, self).get_context_data(**kwargs)
        context["chart"] = Era.objects.raw("select e.idera, period, count(o.objectid) from Era e join Object o on o.idera = e.idera where period not like 'null' group by e.idera order by count desc limit 15")

        context["table"] = Era.objects.all()[:50]

        return context


class EraViewSet(viewsets.ModelViewSet):
    queryset = Era.objects.all()
    serializer_class = EraSerializer


class MediumViewSet(viewsets.ModelViewSet):
    queryset = Medium.objects.all()
    serializer_class = MediumSerializer


class DepartmentViewSet(viewsets.ModelViewSet):
    queryset = Department.objects.all()
    serializer_class = DepartmentSerializer


class LocationViewSet(viewsets.ModelViewSet):
    queryset = Location.objects.all()
    serializer_class = LocationSerializer


class TypeViewSet(viewsets.ModelViewSet):
    queryset = Type.objects.all()
    serializer_class = TypeSerializer


class ArtistViewSet(viewsets.ModelViewSet):
    queryset = Artist.objects.all()
    serializer_class = ArtistSerializer
