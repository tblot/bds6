from web.models import Era, Medium, Location, Department, Type, Artist
from rest_framework import serializers

class EraSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Era
        fields = ['period', 'dynasty', 'reign']

class MediumSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Medium
        fields = ['label']

class LocationSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Location
        fields = ['culture','city','zone']

class DepartmentSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Department
        fields = ['label']

# class TagSerializer(serializers.HyperlinkedModelSerializer):
#     class Meta:
#         model = Tag
#         fields = ['label']

class TypeSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Type
        fields = ['label']

class ArtistSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Artist
        fields = ['name', 'id_role']

#class Role_ArtistSerializer(serializers.HyperlinkedModelSerializer):
#    class Meta:
#        model = Role_Artist
#        fields = ['label']

