from django.urls import path, include
from rest_framework import routers


from . import views

router = routers.DefaultRouter()
router.register(r'era', views.EraViewSet)
router.register(r'mediumList', views.MediumViewSet)
router.register(r'Location', views.LocationViewSet)
router.register(r'department', views.DepartmentViewSet)
# router.register(r'tag', views.TagViewSet)
router.register(r'type', views.TypeViewSet)
#router.register(r'role_artist', views.Role_ArtistViewSet)
router.register(r'artist', views.ArtistViewSet)

urlpatterns = [
    path('', views.index.as_view(), name='index'),
    path('departement', views.department.as_view(), name='departement'),
    path('artist', views.artist.as_view(), name='artist'),
    path('medium', views.medium.as_view(), name='medium'),
    path('culture', views.culture.as_view(), name='culture'),
    path('epoque', views.epoque.as_view(), name='epoque'),
    path('login', views.login, name='login'),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))

]
urlpatterns += router.urls
