# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Artist(models.Model):
    idartist = models.AutoField(primary_key=True)
    name = models.TextField(blank=True, null=True)
    role = models.TextField(blank=True, null=True)
    # culture = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'artist'


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=150)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    id = models.BigAutoField(primary_key=True)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.BooleanField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    email = models.CharField(max_length=254)
    is_staff = models.BooleanField()
    is_active = models.BooleanField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


# class Classification(models.Model):
#     idclassification = models.AutoField(primary_key=True)
#     label = models.CharField(max_length=255, blank=True, null=True)
#
#     class Meta:
#         managed = False
#         db_table = 'classification'


class Department(models.Model):
    iddepartment = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'department'


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    id = models.BigAutoField(primary_key=True)
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class Era(models.Model):
    idera = models.AutoField(primary_key=True)
    period = models.CharField(max_length=255, blank=True, null=True)
    # dynasty = models.CharField(max_length=255, blank=True, null=True)
    reign = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'era'


class Location(models.Model):
    idlocation = models.AutoField(primary_key=True)
    # locale = models.CharField(max_length=255, blank=True, null=True)
    city = models.CharField(max_length=255, blank=True, null=True)
    # county = models.CharField(max_length=255, blank=True, null=True)
    # river = models.CharField(max_length=255, blank=True, null=True)
    zone = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'location'


class Medium(models.Model):
    idmedium = models.AutoField(primary_key=True)
    label = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'medium'


class Object(models.Model):
    # idobject = models.IntegerField(primary_key=True)
    objectid = models.AutoField(primary_key=True)

    reference = models.CharField(max_length=255, blank=True, null=True)
    title = models.TextField(blank=True, null=True)
    idtype = models.ForeignKey('Type', models.DO_NOTHING, db_column='idtype', blank=True, null=True)
    iddepartment = models.ForeignKey(Department, models.DO_NOTHING, db_column='iddepartment', blank=True, null=True)
    idartist = models.ForeignKey(Artist, models.DO_NOTHING, db_column='idartist', blank=True, null=True)
    idmedium = models.ForeignKey(Medium, models.DO_NOTHING, db_column='idmedium', blank=True, null=True)
    idera = models.ForeignKey(Era, models.DO_NOTHING, db_column='idera', blank=True, null=True)
    # idclassification = models.ForeignKey(Classification, models.DO_NOTHING, db_column='idclassification', blank=True, null=True)
    idlocation = models.ForeignKey(Location, models.DO_NOTHING, db_column='idlocation', blank=True, null=True)
    datebegin = models.IntegerField(blank=True, null=True)
    dateend = models.IntegerField(blank=True, null=True)
    ishighlight = models.BooleanField(blank=True, null=True)
    ispublicdomain = models.BooleanField(blank=True, null=True)
    accessionyear = models.IntegerField(blank=True, null=True)
    linkressource = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'object'


# class ObjectArtistManytomany(models.Model):
#     idobject = models.OneToOneField(Object, models.DO_NOTHING, db_column='idobject', primary_key=True)
#     idartist = models.ForeignKey(Artist, models.DO_NOTHING, db_column='idartist')
#
#     class Meta:
#         managed = False
#         db_table = 'object_artist_manytomany'
#         unique_together = (('idobject', 'idartist'),)
#
#
# class ObjectMediumManytomany(models.Model):
#     idobject = models.OneToOneField(Object, models.DO_NOTHING, db_column='idobject', primary_key=True)
#     idmedium = models.ForeignKey(Medium, models.DO_NOTHING, db_column='idmedium')
#
#     class Meta:
#         managed = False
#         db_table = 'object_medium_manytomany'
#         unique_together = (('idobject', 'idmedium'),)
#
#
# class ObjectTagManytomany(models.Model):
#     idobject = models.OneToOneField(Object, models.DO_NOTHING, db_column='idobject', primary_key=True)
#     idtag = models.ForeignKey('Tag', models.DO_NOTHING, db_column='idtag')
#
#     class Meta:
#         managed = False
#         db_table = 'object_tag_manytomany'
#         unique_together = (('idobject', 'idtag'),)
#
#
# class Tag(models.Model):
#     idtag = models.AutoField(primary_key=True)
#     label = models.CharField(max_length=255, blank=True, null=True)
#
#     class Meta:
#         managed = False
#         db_table = 'tag'


class Type(models.Model):
    idtype = models.AutoField(primary_key=True)
    label = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'type'
