function rainbow(numOfSteps, step) {
    // This function generates vibrant, "evenly spaced" colours (i.e. no clustering). This is ideal for creating easily distinguishable vibrant markers in Google Maps and other apps.
    // Adam Cole, 2011-Sept-14
    // HSV to RBG adapted from: http://mjijackson.com/2008/02/rgb-to-hsl-and-rgb-to-hsv-color-model-conversion-algorithms-in-javascript
    var r, g, b;
    var h = step / numOfSteps;
    var i = ~~(h * 6);
    var f = h * 6 - i;
    var q = 1 - f;
    switch(i % 6){
        case 0: r = 1; g = f; b = 0; break;
        case 1: r = q; g = 1; b = 0; break;
        case 2: r = 0; g = 1; b = f; break;
        case 3: r = 0; g = q; b = 1; break;
        case 4: r = f; g = 0; b = 1; break;
        case 5: r = 1; g = 0; b = q; break;
    }
    var c = "#" + ("00" + (~ ~(r * 255)).toString(16)).slice(-2) + ("00" + (~ ~(g * 255)).toString(16)).slice(-2) + ("00" + (~ ~(b * 255)).toString(16)).slice(-2);
    return (c);
}

function donutChart(elementName, elementLabel, data) {
    let values = Object.values(data)[0]
    let amount = Object.values(data)[1]
    let labels = []
    for (let i = 0; i < Object.keys(data).length; i++) {
        labels.push(Object.keys(data)[i].slice(1))
    }
    let ctx = document.getElementById(elementName).getContext('2d');
    let htmlLabel = document.getElementById(elementLabel)
    let colorPalette = []
    for (let i = 0; i < values.length; i++) {
        colorPalette.push(rainbow(amount.length, i));
        htmlLabel.innerHTML +=  '<div class="chart-note mr-0 d-block"><span style="background: '+colorPalette[i]+';" class="dot"></span><span>'+values[i]+'</span></div>'
    }
    new Chart(ctx, {
        type: 'doughnut',
        data: {
            labels: values
            , //loop through queryset,
            datasets: [{
                data: amount,
                backgroundColor: colorPalette,
                borderWidth: 1
            }],
        },
        options: {
            responsive: true,
            scales: {
                y: {
                    beginAtZero: true
                }
            },
            legend: {
                display: false
            },
            animation: {
                animateScale: true,
                animateRotate: true
            },
            tooltips: {
                titleFontFamily: "Poppins",
                xPadding: 15,
                yPadding: 10,
                caretPadding: 0,
                bodyFontSize: 16
            }
        }
    })
}

function pieChart(elementName, elementLabel, data) {
    let values = Object.values(data)[0]
    let amount = Object.values(data)[1]
    let labels = []
    for (let i = 0; i < Object.keys(data).length; i++) {
        labels.push(Object.keys(data)[i].slice(1))
    }
    let ctx = document.getElementById(elementName).getContext('2d');
    let htmlLabel = document.getElementById(elementLabel)
    let colorPalette = []
    for (let i = 0; i < values.length; i++) {
        colorPalette.push(rainbow(amount.length, i));
        htmlLabel.innerHTML +=  '<div class="chart-note mr-0 d-block"><span style="background: '+colorPalette[i]+';" class="dot"></span><span>'+values[i]+'</span></div>'
    }
    new Chart(ctx, {
        type: 'pie',
        data: {
            labels: values
            , //loop through queryset,
            datasets: [{
                data: amount,
                backgroundColor: colorPalette,
                borderWidth: 1
            }],
        },
        options: {
            responsive: true,
            scales: {
                y: {
                    beginAtZero: true
                }
            },
            legend: {
                display: false
            },
            animation: {
                animateScale: true,
                animateRotate: true
            },
            tooltips: {
                titleFontFamily: "Poppins",
                xPadding: 15,
                yPadding: 10,
                caretPadding: 0,
                bodyFontSize: 16
            }
        }
    })
}

function histoChart(elementName, elementLabel, data) {
    let valuesLabel = []
    let values = Object.values(data)[0]
    let amount = Object.values(data)[1]
    let labels = []
    for (let i = 0; i < Object.keys(data).length; i++) {
        labels.push(Object.keys(data)[i].slice(1))
    }
    let ctx = document.getElementById(elementName).getContext('2d');
    let htmlLabel = document.getElementById(elementLabel)
    let colorPalette = []
    for (let i = 0; i < values.length; i++) {
        colorPalette.push(rainbow(amount.length, i));
        valuesLabel.push(' ')
        htmlLabel.innerHTML +=  '<div class="chart-note mr-0 d-block"><span style="background: '+colorPalette[i]+';" class="dot"></span><span>'+values[i]+'</span></div>'
    }
    new Chart(ctx, {
        type: 'bar',
        data: {
            labels: valuesLabel,
            datasets: [{
                axis: 'y',
                indexAxis: 'y',
                data: amount,
                backgroundColor: colorPalette,
                borderWidth: 1,

            }],
        },
        options: {
            indexAxis: 'y',
            responsive: true,
            scales: {
                y: {
                    beginAtZero: true
                }
            },
            legend: {
                display: false
            },
            animation: {
                animateScale: true,
                animateRotate: true
            },
            tooltips: {
                titleFontFamily: "Poppins",
                xPadding: 15,
                yPadding: 10,
                caretPadding: 0,
                bodyFontSize: 16
            }
        }
    })
}

function multiChart(elementName, elementLabel, data) {
    let ctx = document.getElementById(elementName).getContext('2d');
    new Chart(ctx, {
        type: 'bar',
        data: {
            labels: [
                'January',
                'February',
                'March',
                'April'
            ],
            datasets: [{
                type: 'line',
                label: 'Bar Dataset',
                data: [10, 20, 30, 40],
                borderColor: 'rgb(255, 99, 132)',
                fill: false,
            }, {
                type: 'line',
                label: 'Line Dataset',
                data: [15, 25, 12, 17],
                fill: false,
                borderColor: 'rgb(54, 162, 235)'
            }]
        },
        options: {
            indexAxis: 'y',
            responsive: true,
            scales: {
                y: {
                    beginAtZero: true
                }
            },
            legend: {
                display: false
            },
            animation: {
                animateScale: true,
                animateRotate: true
            },
            tooltips: {
                titleFontFamily: "Poppins",
                xPadding: 15,
                yPadding: 10,
                caretPadding: 0,
                bodyFontSize: 16
            }
        }
    });
}

async function getPage(link) {
        let response = await fetch(`https://collectionapi.metmuseum.org/public/collection/v1/objects/${link}`)
		return await response.json();
    }