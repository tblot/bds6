
```sql
drop table if exists museum;

CREATE TABLE museum (
objetNumber varchar(255),
isHighlight boolean,
isTimeLineWork boolean,
isPublicDomain boolean,
objectId integer,
galleryNumber varchar(255),
department varchar(255),
accessionYear varchar(255),
objectName varchar(255),
title text,
culture varchar(255),
period varchar(255),
dynasty varchar(255),
reign varchar(255),
portfolio text,
constituentId numeric,
artistRole text,
artistPrefix text,
artistDisplayName text,
artistDisplayBio text,
artistSuffix text,
artistAlphaSort text,
artistNationality text,
artistBeginDate text,
artistEndDate text,
artistGender text,
artistUlanUrl text,
artistWikiDataUrl text,
objectDate varchar(255),
objectBeginDate integer,
objectEndDate integer,
medium text,
dimensions text,
creditLine text,
geographyType varchar(255),
city varchar(255),
state varchar(255),
county varchar(255),
country varchar(255),
Region varchar(255),
subRegion varchar(255),
locale varchar(255),
locus varchar(255),
excavation varchar(255),
river varchar(255),
classification varchar(255),
rightsAndReproduction varchar(255),
linkResource varchar(255),
objectWikiDataurl varchar(255),
metaDataDate varchar(255),
repository varchar(255),
tags varchar(255),
tagsAATUrl text,
tagsWikidataURL text
);
```

```plsql
\encoding utf8;
\copy museum from 'cheminVersLeFichier\MetObjects.csv' csv header;
```

```sql
ALTER TABLE museum
DROP COLUMN istimelinework,
DROP COLUMN gallerynumber,
DROP COLUMN constituentId,
DROP COLUMN artistPrefix,
DROP COLUMN artistDisplayBio,
DROP COLUMN artistSuffix,
DROP COLUMN artistAlphaSort,
DROP COLUMN artistNationality,
DROP COLUMN artistBeginDate,
DROP COLUMN artistEndDate,
DROP COLUMN portfolio,
DROP COLUMN artistgender,
DROP COLUMN artistulanurl,
DROP COLUMN artistwikidataurl,
DROP COLUMN creditline,
DROP COLUMN geographytype,
DROP COLUMN state,
DROP COLUMN subregion,
DROP COLUMN locus,
DROP COLUMN excavation,
DROP COLUMN rightsandreproduction,
DROP COLUMN objectwikidataurl,
DROP COLUMN metadatadate,
DROP COLUMN repository,
DROP COLUMN tagsaaturl,
DROP COLUMN tagswikidataurl,
DROP COLUMN objectDate;
```
```sql
delete from museum where objectenddate < objectbegindate and objectbegindate > 0;
delete from museum where objetnumber is null or objectid is null or title is null or medium is null;
delete from museum where culture is null and period is null and dynasty is null and reign is null and country is null;
	
